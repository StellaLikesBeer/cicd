# Dockerfile, Image, Container
FROM python:3.9-slim-bullseye

ADD hello_stella.py .

# install dependency if needed 
# RUN pip install numpy pandas ...

CMD [ "python", "./hello_stella.py" ]
